const express = require("express");
const {
  gaccountControls,
  gIsiKeranjang,
  gPembeli,
  gPesantren,
  gProduk,
  gRiwayat,
  gTransaksi,
  createAccounts,
  pProduk,
} = require("../controller/publicControl");
const router = express.Router();

router.get('/accounts', gaccountControls)
      .get('/keranjang', gIsiKeranjang)
      .post('/register', createAccounts)
      .get('/pembeli', gPembeli)
      .get('/pesantren', gPesantren)
      .get('/produk', gProduk)
      .post('/produk', pProduk)
      .get('/riwayat', gRiwayat)
      .get('/transaksi', gTransaksi)
      

module.exports = router;
