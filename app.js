const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const publicRoute = require('./routes/publicRoute')
const multer  = require('multer')
const upload = multer()
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
app.use(upload.any())
const PORT = 4444 ;

app.use("/public", publicRoute);

app.listen(PORT, () => {
  console.log(`listening on port ${PORT}`);
});
