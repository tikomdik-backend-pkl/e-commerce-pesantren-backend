var DataTypes = require("sequelize").DataTypes;
var _account_controls = require("./account_controls");
var _keranjang_pembeli = require("./keranjang_pembeli");
var _pesantren_identitas = require("./pesantren_identitas");
var _produk = require("./produk");
var _riwayat = require("./riwayat");
var _transaksi = require("./transaksi");
var _user_pembeli = require("./user_pembeli");

function initModels(sequelize) {
  var account_controls = _account_controls(sequelize, DataTypes);
  var keranjang_pembeli = _keranjang_pembeli(sequelize, DataTypes);
  var pesantren_identitas = _pesantren_identitas(sequelize, DataTypes);
  var produk = _produk(sequelize, DataTypes);
  var riwayat = _riwayat(sequelize, DataTypes);
  var transaksi = _transaksi(sequelize, DataTypes);
  var user_pembeli = _user_pembeli(sequelize, DataTypes);

  produk.belongsTo(pesantren_identitas, { as: "pesantren", foreignKey: "akun_id"});
  pesantren_identitas.hasMany(produk, { as: "produks", foreignKey: "akun_id"});
  keranjang_pembeli.belongsTo(produk, { as: "produk", foreignKey: "produk_id"});
  produk.hasMany(keranjang_pembeli, { as: "keranjang_pembelis", foreignKey: "produk_id"});

  return {
    account_controls,
    keranjang_pembeli,
    pesantren_identitas,
    produk,
    riwayat,
    transaksi,
    user_pembeli,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
