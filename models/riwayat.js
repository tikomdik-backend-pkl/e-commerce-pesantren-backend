const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('riwayat', {
    riwayat_id: {
      type: DataTypes.UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    produk_id: {
      type: DataTypes.UUID,
      allowNull: true
    },
    akun_id: {
      type: DataTypes.UUID,
      allowNull: true
    },
    tanggal_transaksi: {
      type: DataTypes.DATEONLY,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'riwayat',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "riwayat_pkey",
        unique: true,
        fields: [
          { name: "riwayat_id" },
        ]
      },
    ]
  });
};
