const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('produk', {
    produk_id: {
      type: DataTypes.UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    nama_produk: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    harga: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    deskripsi: {
      type: DataTypes.STRING,
      allowNull: false
    },
    kategori: {
      type: DataTypes.STRING,
      allowNull: true
    },
    detail: {
      type: DataTypes.STRING,
      allowNull: true
    },
    gambar: {
      type: DataTypes.BLOB,
      allowNull: false
    },
    akun_id: {
      type: DataTypes.UUID,
      allowNull: true,
      references: {
        model: 'pesantren_identitas',
        key: 'akun_id'
      }
    }
  }, {
    sequelize,
    tableName: 'produk',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "produk_pkey",
        unique: true,
        fields: [
          { name: "produk_id" },
        ]
      },
    ]
  });
};
