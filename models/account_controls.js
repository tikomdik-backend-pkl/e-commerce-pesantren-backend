const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('account_controls', {
    akun_id: {
      type: DataTypes.UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    username: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    password: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    level: {
      type: DataTypes.DECIMAL,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'account_controls',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "account_controls_pkey",
        unique: true,
        fields: [
          { name: "akun_id" },
        ]
      },
    ]
  });
};
