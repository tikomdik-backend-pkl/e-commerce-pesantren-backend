const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('keranjang_pembeli', {
    isi_keranjang_id: {
      type: DataTypes.UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    keranjang_id: {
      type: DataTypes.UUID,
      allowNull: true
    },
    produk_id: {
      type: DataTypes.UUID,
      allowNull: true,
      references: {
        model: 'produk',
        key: 'produk_id'
      }
    },
    jumlah: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'keranjang_pembeli',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "id_keranjang_pk",
        unique: true,
        fields: [
          { name: "isi_keranjang_id" },
        ]
      },
    ]
  });
};
