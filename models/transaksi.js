const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('transaksi', {
    transaksi_id: {
      type: DataTypes.UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    akun_Id: {
      type: DataTypes.UUID,
      allowNull: false
    },
    keranjang_id: {
      type: DataTypes.UUID,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'transaksi',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "transaksi_pkey",
        unique: true,
        fields: [
          { name: "transaksi_id" },
        ]
      },
    ]
  });
};
