const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('pesantren_identitas', {
    akun_id: {
      type: DataTypes.UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    nama_pesantren: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    alamat_jalan: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    rt: {
      type: DataTypes.CHAR(5),
      allowNull: true
    },
    rw: {
      type: DataTypes.CHAR(5),
      allowNull: true
    },
    dusun_kampung: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    desa_kelurahan: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    kabupaten_kota: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    provinsi: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    no_kontak: {
      type: DataTypes.STRING(20),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'pesantren_identitas',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "pesantren_identitas_pkey",
        unique: true,
        fields: [
          { name: "akun_id" },
        ]
      },
    ]
  });
};
