const {findAll, createAccount, Create} = require('.')
const fs = require('fs')

exports.gaccountControls = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('account_controls', properties)
    res.send(data)
}
exports.createAccounts = async (req, res) => {
    const payload = req.body
    const data = await createAccount(payload)
    if (data.error) {
        return res.status(data.status).send(data.error);
      }
    res.send(data)
}
exports.gIsiKeranjang = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('keranjang_pembeli', properties)
    res.send(data)
}
exports.gPesantren = async (req, res) => {
  const page = req.query.page || 0;
  const limit = req.query.limit || 5;
  const properties = { limit: limit, offset: limit * page };

  const data = await findAll("pesantren_identitas", properties);
  res.send(data);
};
exports.gProduk = async (req, res) => {
  const page = req.query.page || 0;
  const limit = req.query.limit || 5;
  const properties = { limit: limit, offset: limit * page };

  const data = await findAll("produk", properties);
  data.data.forEach((i) => {
    let base64;
    if (i.gambar) {
      base64 = Buffer.from(i.gambar).toString("base64");
    }
    i.gambar = base64 || null;
  });
  res.send(data);
};
exports.pProduk = async (req, res) => {
    const payload = req.body
    payload['gambar'] = req.files[0].buffer
    const data = await Create('produk',payload)
    if (data.error) {
        return res.status(data.status).send(data.error);
      }
    res.send(data)
}
exports.gRiwayat = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('riwayat', properties)
    res.send(data)
}
exports.gTransaksi = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('transaksi', properties)
    res.send(data)
}
exports.gPembeli = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('user_pembeli', properties)
    res.send(data)
}