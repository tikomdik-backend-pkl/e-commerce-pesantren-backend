const sequelize = require("../models");
const initModels = require("../models/init-models");

const models = initModels(sequelize);
const bcrypt = require("bcryptjs");
const findAll = async (modelName, properties) => {
  try {
    const Model = models[modelName];

    let total_rows = await Model.count();
    let total_page = Math.ceil(total_rows / properties.limit);
    const data = await Model.findAll(properties);
    return { data: data, total_page: total_page, total_rows: total_rows };
  } catch (err) {
    console.error(err);
    return `Server error, ${err}`;
  }
};
const Create = async (modelName, payload) => {
  try {
    const Model = models[modelName];

    const data = await Model.create(payload);
    return { data: data };
  } catch (err) {
    console.error(err);
    return { error: `Server error, ${err}`, status: 500 };
  }
};

const Delete = async (modelName, properties) => {
  try {
    const Model = models[modelName];

    const data = await Model.destroy(properties);
    return { data: data };
  } catch (err) {
    console.error(err);
    return `Server error, ${err}`;
  }
};

const Update = async (modelName, properties) => {
  try {
    // properties = { where: {*nama kolumn* : *data*}, *property lain* }
    // ^^ Reminder bentuk variable properties untuk update ^^
    const Model = models[modelName];

    const data = await Model.update(properties);
    return { data: data };
  } catch (err) {
    console.error(err);
    return `Server error, ${err}`;
  }
};

const createAccount = async (payload) => {
  try {
    let modelName;
    if (payload.pendaftar === "pembeli")
      (modelName = "user_pembeli"), (payload["level"] = 1);
    else (modelName = "pesantren_identitas"), (payload["level"] = 2);

    const Model = models[modelName];

    if (payload.username == null || payload.username == "") throw "Lengkapi username anda";
    else if (payload.password == null || payload.password == "") throw "Lengkapi password anda";
    else if (payload.namalengkap == null || payload.namalengkap == "") throw "Lengkapi nama lengkap anda";
    const data = await Model.create(payload);
    try {
      const hash = await bcrypt.hash(payload.password, 12);
      payload["akun_id"] = data.akun_id;
      payload["password"] = hash;

      const acc_credentials = await models.account_controls.create(payload);
      return {
        data: data,
        credentials: acc_credentials,
        message: "Berhasil mendaftar",
      };
    } catch (err) {
      console.error(err);
      return { error: `Server error, ${err}`, status: 500 };
    }
  } catch (err) {
    console.error(err);
    return { error: `Input error, ${err}`, status: 400 };
  }
};

module.exports = { findAll, Create, Update, Delete, createAccount };
